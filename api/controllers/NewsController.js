/**
 * NewsController
 *
 * @description :: Server-side logic for managing news
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {


    /**
     * `NewsController.getAllNews()`
     */
    getAllNews: function (req, res) {
        /*NewsService.createNews({
            url: "http",
            title: "hey",
            description: "test",
            pubDate: "Mon, 27 Jul 2015 19:28:00 GMT",
            published: false
        }, function (news) {
            //console.log(news);
            return res.json([news]);
        });*/
        var source = req.swagger.params.source.value;

        NewsService.getNewsOneMonth(source, res);
    },

    updateDB: function(req, res) {
        NewsService.updateDB();
        res.ok();
    },

    updateNews: function(req, res) {
       var news = req.swagger.params.news.value;
       NewsService.updateNews(news, res);
    },

    /**
     * `NewsController.getNewsById()`
     */
    getNewsById: function (req, res) {
        return res.json({
            url: "http",
            title: "hey",
            description: "test",
            pubDate: "Mon, 27 Jul 2015 19:28:00 GMT"
        });
    }
};

