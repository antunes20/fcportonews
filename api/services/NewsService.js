/**
 * Created by Jose on 29/07/2015.
 */

var FeedParser = require('feedparser')
    , request = require('request')
    , Iconv = require('iconv').Iconv
    , sanitizeHtml = require('sanitize-html')
    , striptags = require('striptags')
    , YQL = require('yql')
    , wordsFilter = ['FC Porto', 'Dragões', 'Dragão', 'dragão', 'Lopetegui', 'Pinto da Costa']
    , sources = ['OJogo.pt', 'Notícias zerozero.pt', 'FC Porto']
    , sourcesFunc = [getOJogoNews, getZeroZeroNews, getFCPortoOfficialNews]
    , Entities = require('html-entities').AllHtmlEntities
    , OJOGO = 0
    , ZEROZERO = 1
    , FCPORTO = 2


var minutes = 15, the_interval = minutes * 60 * 1000;

setInterval(function () {
    module.exports.updateDB();
    // do your stuff here
}, the_interval);

module.exports = {
    createNews: function (news, cb) {
        News.create(news).exec(function (err, news) {
            if (err) cb("error");
            else cb(news);
        });
    },

    getNewsOneMonth: function (source, res) {
        var date = new Date();
        date.setHours(0, 0, 0, 0);
        date.setMonth(date.getMonth() - 1);

        if (source == 'ojogo') {
            source = sources[OJOGO];
        }
        else if (source == 'zerozero') {
            source = sources[ZEROZERO];
        }
        else if (source == 'fcporto') {
            source = sources[FCPORTO];
        }

        if (source) {
            News.find({pubDate: {'>=': date}, source: source, sort: 'pubDate DESC'})
                .then(function (news) {
                    return res.json(news);
                })
                .catch(function (error) {
                    console.log(error);
                    return res.error('Something went wrong. Please try again');
                });
        }
        else {
            News.find({pubDate: {'>=': date}, sort: 'pubDate DESC'})
                .then(function (news) {
                    return res.json(news);
                })
                .catch(function (error) {
                    console.log(error);
                    return res.error('Something went wrong. Please try again');
                });
        }
    },

    updateDB: function () {
        sourcesFunc.forEach(function (func) {
            func();
        });
    },

    updateNews: function (news, res) {
        News.update({id: news.id}, {published: true})
            .then(function (updatedNews) {
                res.json(updatedNews[0]);
            })
            .catch(function (error) {
                console.log(error);
                res.error('Something went wrong. Please try again');
            })
    },
    test: function () {
        getFCPortoOfficialNews();
    }
};

function getFCPortoOfficialNews() {
    var query = new YQL("select * from html where url=\"http://www.fcporto.pt/pt/noticias/Pages/noticias.aspx\"  and xpath='//*[@id=\"listagem\"]/*[@class=\"item\"]/*[@class=\"cont\"]'");
    query.exec(function (error, response) {
        //console.log(response.query.results);
        response.query.results.div.forEach(function (item) {
            //console.log(item);
            News.create({
                url: item.a.href,
                title: item.h2.a.content,
                description: item.p,
                pubDate: new Date(),
                published: false,
                source: sources[FCPORTO]
            }).then(function (news) {
                //console.log(news);
            }).catch(function (error) {
                console.log("Already created!");
            });
        });
    });
}

function getOJogoNews() {
    var feedparser = new FeedParser();
    var req = request('http://feeds.ojogo.pt/OJ-Porto');
    feedparser.on('readable', readStream);
    req.on('error', function (error) {
        // handle any request errors
    });
    req.on('response', function (res) {
        if (res.statusCode != 200) return this.emit('error', new Error('Bad status code'));
        var charset = getParams(res.headers['content-type'] || '').charset;
        //DataHolderService.lastChecked = new Date();
        res = maybeTranslate(res, charset);
        // And boom goes the dynamite
        res.pipe(feedparser);
    });
}

function getZeroZeroNews() {
    var feedparser = new FeedParser();
    var req = request('http://www.zerozero.pt/rss/noticias.php');
    feedparser.on('readable', readStreamWithFilter);
    req.on('error', function (error) {
        // handle any request errors
    });
    req.on('response', function (res) {
        if (res.statusCode != 200) return this.emit('error', new Error('Bad status code'));
        //var charset = getParams(res.headers['content-type'] || '').charset;
        var charset = 'windows-1252';
        //DataHolderService.lastChecked = new Date();
        res = maybeTranslate(res, charset);
        // And boom goes the dynamite
        res.pipe(feedparser);
    });
}

function readStream() {
    var entities = new Entities();
    // This is where the action is!
    var stream = this
        , meta = this.meta // **NOTE** the "meta" is always available in the context of the feedparser instance
        , item;
    var source;
    var index;
    for (index = 0; index < sources.length; ++index) {
        if (meta.title == sources[index]) {
            source = sources[index];
            break;
        }
    }
    while (item = stream.read()) {
        //console.log(item);

        News.create({
            url: item.link,
            title: entities.decode(item.title),
            description: entities.decode(striptags(item.description)),
            pubDate: item.pubdate,
            published: false,
            source: source
        }).then(function (news) {
            //console.log(news);
        }).catch(function (error) {
            console.log("Already created!");
        });
    }
}

function readStreamWithFilter() {
    var entities = new Entities();
    // This is where the action is!
    var stream = this
        , meta = this.meta // **NOTE** the "meta" is always available in the context of the feedparser instance
        , item;
    var source;
    var index;
    for (index = 0; index < sources.length; ++index) {
        if (meta.title == sources[index]) {
            source = sources[index];
            break;
        }
    }
    while (item = stream.read()) {
        var title, description;
        var isPortoNews = false;
        var index;
        item.title = entities.decode(item.title);
        if (item.description != null)
            item.description = entities.decode(item.description);
        for (index = 0; index < wordsFilter.length; ++index) {
            if (item.title.indexOf(wordsFilter[index]) > -1 || (item.description != null && item.description.indexOf(wordsFilter[index]) > -1)) {
                isPortoNews = true;
                break;
            }
        }

        if (isPortoNews) {
            News.create({
                url: item.link,
                title: item.title,
                description: striptags(item.description),
                pubDate: item.pubdate,
                published: false,
                source: source
            }).then(function (news) {
                //console.log(news);
            }).catch(function (error) {
                console.log("Already created!");
            });
        }
    }
}

function maybeTranslate(res, charset) {
    var iconv;
    // Use iconv if its not utf8 already.
    if (!iconv && charset && !/utf-*8/i.test(charset)) {
        try {
            iconv = new Iconv(charset, 'utf-8');
            console.log('Converting from charset %s to utf-8', charset);
            iconv.on('error', done);
            // If we're using iconv, stream will be the output of iconv
            // otherwise it will remain the output of request
            res = res.pipe(iconv);
        } catch (err) {
            res.emit('error', err);
        }
    }
    return res;
}

function getParams(str) {
    var params = str.split(';').reduce(function (params, param) {
        var parts = param.split('=').map(function (part) {
            return part.trim();
        });
        if (parts.length === 2) {
            params[parts[0]] = parts[1];
        }
        return params;
    }, {});
    return params;
}

function done(err) {
    if (err) {
        console.log(err, err.stack);
        return process.exit(1);
    }
    process.exit();
}