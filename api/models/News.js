/**
* News.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

  attributes: {

    url : { type: 'string', unique: true },

    title : { type: 'string' },

    description : { type: 'string' },

    pubDate : { type: 'datetime' },

    published : {type: 'boolean'},

    source : { type: 'string' }
  }
};

