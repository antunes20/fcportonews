'use strict';

// Declare app level module which depends on views, and components
angular.module('myApp', [
    'ngRoute',
    'myApp.news',
    'myApp.ojogo',
    'myApp.version',
    'angular-loading-bar',
    'myApp.zerozero',
    'myApp.fcporto'
]).
    config(['$routeProvider', function ($routeProvider) {
        $routeProvider.otherwise({redirectTo: '/news'});
    }]);
