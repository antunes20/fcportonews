'use strict';

angular.module('myApp.fcporto', ['ngRoute', 'datatables'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/fcporto', {
            templateUrl: 'fcporto/fcporto.html',
            controller: 'FCPortoCtrl'
        });
    }])

    .controller('FCPortoCtrl', ['$scope', '$http',
        function ($scope, $http) {
            $scope.dtOptions = {
                order: [[2, "desc"]]
            };
            var url = "/news?source=fcporto";
            $http.get(url)
                .then(function (response) {
                    $scope.allnews = response.data;
                });
            $scope.setPublished = function (row) {
                var url = "/news/" + row.id;
                //row.published = true;
                $http.put(url, row)
                    .then(function (response) {
                        row.published = response.data.published;
                        $scope.dtInstance.rerender();
                    });
            }

            $scope.dtInstance = {};

        }]);