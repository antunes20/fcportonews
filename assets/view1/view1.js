'use strict';

angular.module('myApp.news', ['ngRoute', 'datatables'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/news', {
            templateUrl: 'view1/view1.html',
            controller: 'View1Ctrl'
        });
    }])

    .controller('View1Ctrl', ['$scope', '$http',
        function ($scope, $http) {
            $scope.dtOptions = {
                order: [[2, "desc"]]
            };
            var url = "/news";
            $http.get(url)
                .then(function (response) {
                    $scope.allnews = response.data;
                });
            $scope.setPublished = function (row) {
                var url = "/news/" + row.id;
                //row.published = true;
                $http.put(url, row)
                    .then(function (response) {
                        row.published = response.data.published;
                        $scope.dtInstance.rerender();
                    });
            }

            $scope.dtInstance = {};

        }]);