'use strict';

angular.module('myApp.zerozero', ['ngRoute', 'datatables'])

    .config(['$routeProvider', function ($routeProvider) {
        $routeProvider.when('/zerozero', {
            templateUrl: 'zerozero/zerozero.html',
            controller: 'ZeroZeroCtrl'
        });
    }])

    .controller('ZeroZeroCtrl', ['$scope', '$http',
        function ($scope, $http) {
            $scope.dtOptions = {
                order: [[2, "desc"]]
            };
            var url = "/news?source=zerozero";
            $http.get(url)
                .then(function (response) {
                    $scope.allnews = response.data;
                });
            $scope.setPublished = function (row) {
                var url = "/news/" + row.id;
                //row.published = true;
                $http.put(url, row)
                    .then(function (response) {
                        row.published = response.data.published;
                        $scope.dtInstance.rerender();
                    });
            }

            $scope.dtInstance = {};

        }]);